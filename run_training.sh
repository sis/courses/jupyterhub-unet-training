#!/bin/bash

module purge
module load gcc/8.2.0 python_gpu/3.10.4 hdf5/1.10.1 eth_proxy
source /cluster/project/sis/cdss/oschmanf/segmentation_training/midap/euler/midap/bin/activate
FOLDER=my_weights/
sbatch --mem-per-cpu=12g --gpus=1 --gres=gpumem:12g --time=01:00:00 --mail-type=END,FAIL --wrap "python /cluster/project/sis/cdss/oschmanf/segmentation_training/midap/training/train.py --n_grid $1 --n_repeats $2 --epochs 50 --image_size $3 $4 $5 --tfboard_logdir $6 --save_path $FOLDER $7/*raw.tif"

