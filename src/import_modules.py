import datetime
import ipywidgets as widgets
import os
from glob import glob
import numpy as np
import matplotlib.pyplot as plt

from skimage import io
from skimage.segmentation import mark_boundaries
from skimage.measure import label

import subprocess
from typing import Union

class ImageLoader(object):
    def __init__(self, folder_name: Union[str, os.PathLike], min_img_shape = (64, 64, 1)):
        """
        :param folder_name: Name of folder containing pairs of raw images and segmentations for training.
        """
        self.folder_name = folder_name
        self.min_img_shape = min_img_shape

    def get_file_names(self):
        """
        Gets names of raw images and segmentations in folder
        """
        self.raw_images = glob(self.folder_name+'/*raw.tif')
        self.seg_images = [r.replace('raw', 'seg') for r in self.raw_images]

    def display_images(self):
        """
        Displays overlay of raw images and segmentations.
        """
        plt.figure(figsize=(10,15))
        for r, s in zip(self.raw_images, self.seg_images):
            plt.figure()
            img = io.imread(r)
            seg = io.imread(s)
            io.imshow(mark_boundaries(img, label(seg)))
            plt.xticks([])
            plt.yticks([])
            plt.title(r)
        plt.show()

    def compute_train_parameters(self):
        """
        Computes maximal number of grids for image tiling based on image size.
        """
        img_shapes = [io.imread(r).shape for r in self.raw_images]
        self.n_grid = int(np.min(img_shapes)/self.min_img_shape[0])
        self.n_repeats = int(20_000/self.n_grid)


class Buttons(object):

    def button_show_files(self):
        """
        Generates dropdown menu with folder names.
        """
        list_folders = [name for name in os.listdir() if os.path.isdir(name)]
        self.dropdown_files = widgets.Dropdown(options=list_folders)

    def button_check_image_size(self):
        """
        Checks if images are large enough for U-Net training.
        """

        button = widgets.Button(description="Check image size")
        output = widgets.Output()

        display(button, output)

        def on_button_clicked(b):
            self.folder_name = self.dropdown_files.value
            il = ImageLoader(self.folder_name)
            il.get_file_names()

            img = io.imread(il.raw_images[0])

            for i in range(2):
                if il.min_img_shape[i] > img.shape[i]:
                    raise ValueError(f"The image_size of {img.shape} is not compatible with the training data, "
                                    f"max possible shape is {tuple(il.min_img_shape)}!")
                else:
                    print(u'\u2713' + ' Training data have correct size for U-Net training.')

        return button.on_click(on_button_clicked)

    def button_show_images(self):
        """
        Generates button which generates on click the overlays of raw images and segmentations.
        """
        
        button = widgets.Button(description="Show images")
        output = widgets.Output()

        display(button, output)
        
        def on_button_clicked(b):
            self.folder_name = self.dropdown_files.value
            il = ImageLoader(self.folder_name)
            il.get_file_names()
            with output:
                il.display_images()

        return button.on_click(on_button_clicked)
    
    def create_logfolder_name(self):
        """
        Creates folder for train logs with time stemp
        """
        self.logdir = os.path.join("logs", datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))

    def button_start_training(self):
        """
        Creates button which on click starts training.
        """
        button_train = widgets.Button(description="Start training")
        display(button_train)

        def on_button_clicked(b):
            il = ImageLoader(self.folder_name)
            il.get_file_names()
            il.compute_train_parameters()
            self.create_logfolder_name()

            rc = subprocess.call("./run_training.sh " + str(il.n_grid) + " " + str(il.n_repeats) + " " + str(il.min_img_shape[0]) + " " + str(il.min_img_shape[1]) + " " + str(il.min_img_shape[2]) + " " + self.logdir + " " + self.folder_name, shell=True)

        return button_train.on_click(on_button_clicked)
